@ECHO off
REM *******************************
REM * host編集
REM * @ Rev: 1.0.1
REM * @ author: y.ohta
REM *******************************

REM hostファイルをメモ帳（管理者権限）で開く
powershell -NoProfile -ExecutionPolicy unrestricted -Command "start notepad C:\Windows\System32\drivers\etc\hosts -verb runas"
