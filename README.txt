/**
* @ title:  Windows環境のhostsファイルをnotepadで開く
* @ author: y.ohta（https://auto-you-skit.com）
* @ create: 2016.05.04
* @ update: 2016.05.04
* @ rev.:   1.0.0
**/




【0. 概要】

(0-1)
本バッチファイルをクリックするだけで hostsファイルをnotepadで開く



【1. 内容】

(1-1)
host_edit.bat
 … 本体ファイル



【2. 使用方法】

(2-1)
バッチファイルをダブルクリックして開くと、一瞬コマンドプロンプトが開いた後、hostsファイルがnotepadで開かれる。